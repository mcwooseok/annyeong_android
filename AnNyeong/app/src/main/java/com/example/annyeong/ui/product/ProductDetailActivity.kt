package com.example.annyeong.ui.product

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageButton
import com.example.annyeong.R

class ProductDetailActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        var closeBtn = findViewById<AppCompatImageButton>(R.id.btn_product_detail_close)
        closeBtn.setOnClickListener {
            finish()
        }

    }
}