package com.example.annyeong.ui.category

import android.os.Bundle
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.annyeong.R
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.saber.stickyheader.stickyData.HeaderData
import com.saber.stickyheader.stickyView.StickHeaderItemDecoration

class CategoryProductsActivity: AppCompatActivity() {

    private lateinit var categoryProductsViewModel: CategoryProductsViewModel

    private lateinit var categoryProductsRecyclerView: RecyclerView
    private lateinit var categoryProductsLayoutManager: GridLayoutManager
    private lateinit var categoryProductsAdapter: CategoryProductsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_products)

        val toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        var collapsingToolbar = findViewById<CollapsingToolbarLayout>(R.id.collapsing_toolbar)
        collapsingToolbar.title = "간 건강"

        categoryProductsViewModel = CategoryProductsViewModel()
        categoryProductsAdapter = CategoryProductsAdapter(this, categoryProductsViewModel.products)

        categoryProductsLayoutManager = GridLayoutManager(this, 2)

        categoryProductsRecyclerView = findViewById<RecyclerView>(R.id.rv_category_products).apply {
            this.adapter = categoryProductsAdapter
            this.layoutManager = categoryProductsLayoutManager
            addItemDecoration(CategoryProductsDecoration())
        }
    }
}