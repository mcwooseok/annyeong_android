package com.example.annyeong.ui.category

import androidx.lifecycle.ViewModel
import com.example.annyeong.model.Product

class CategoryProductsViewModel : ViewModel() {

    val products: List<Product> = listOf(
        Product("", false, 0, "", "", 4.3f, 98),
        Product("", true, 0, "", "", 4.7f, 2),
        Product("", false, 0, "", "", 4.1f, 24),
        Product("", true, 0, "", "", 2.3f, 64),
        Product("", false, 0, "", "", 3.3f, 234),
        Product("", true, 0, "", "", 4.3f, 88),
        Product("", false, 0, "", "", 4.1f, 1),
        Product("", true, 0, "", "", 4.0f, 0),
        Product("", false, 0, "", "", 4.5f, 23),
        Product("", true, 0, "", "", 2.7f, 46),
        Product("", false, 0, "", "", 1.8f, 75),
        Product("", true, 0, "", "", 5.0f, 72),
        Product("", true, 0, "", "", 4.3f, 11),
        Product("", false, 0, "", "", 4.9f, 2)
    )
}