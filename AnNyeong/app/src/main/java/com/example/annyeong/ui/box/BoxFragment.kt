package com.example.annyeong.ui.box

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.annyeong.R

class BoxFragment : Fragment() {

    private lateinit var boxViewModel: BoxViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        boxViewModel =
                ViewModelProvider(this).get(BoxViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_box, container, false)
        val textView: TextView = root.findViewById(R.id.text_box)
        boxViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}