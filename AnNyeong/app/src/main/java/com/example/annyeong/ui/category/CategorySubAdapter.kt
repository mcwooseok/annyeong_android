package com.example.annyeong.ui.category

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.annyeong.R

class CategorySubAdapter(
        val context: Context,
        val dataSource: List<Category>
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val baseView: View = LayoutInflater.from(context).inflate(R.layout.layout_category_sub_item, parent, false)
        return CategorySubItemHolder(baseView)
    }

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder as CategorySubItemHolder
        holder.iv_thumbnail.setImageDrawable(context.getDrawable(R.drawable.img_search))
        holder.tv_title.text = dataSource[position].title
    }

    class CategorySubItemHolder(
            itemView: View
    ): RecyclerView.ViewHolder(itemView) {
        var iv_thumbnail = itemView.findViewById<ImageView>(R.id.iv_category_sub_thumbnail)
        var tv_title = itemView.findViewById<TextView>(R.id.tv_category_sub_title)
    }
}