package com.example.annyeong.ui.category

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.appcompat.widget.AppCompatImageButton
import androidx.recyclerview.widget.RecyclerView
import com.example.annyeong.R
import com.example.annyeong.model.Product
import com.example.annyeong.ui.product.ProductDetailActivity
import com.saber.stickyheader.stickyData.HeaderData
import com.saber.stickyheader.stickyView.StickHeaderRecyclerView

class CategoryProductsAdapter(
    val context: Context,
    val dataSource: List<Product>
): RecyclerView.Adapter<CategoryProductsAdapter.ProductViewHolder>() {

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val productView: View = LayoutInflater.from(context).inflate(R.layout.layout_category_products_item, parent, false)
        return ProductViewHolder(productView)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bindData(position, context)
    }

    class ProductViewHolder(
        val view: View
    ): RecyclerView.ViewHolder(view) {
        var btn_plus = view.findViewById<AppCompatImageButton>(R.id.iv_category_products_item_plus)
        fun bindData(position: Int, context: Context) {
            view.setOnClickListener {
                var intent = Intent(context, ProductDetailActivity::class.java)
                context.startActivity(intent)
            }
            btn_plus.setOnClickListener {
                Log.d("", "btn plus selected")
            }
        }
    }
}