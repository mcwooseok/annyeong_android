package com.example.annyeong.ui.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.annyeong.R
import com.example.annyeong.utils.toPx

class CategoryFragment : Fragment(), View.OnClickListener {

    private lateinit var categoryViewModel: CategoryViewModel

    private lateinit var categoryRecyclerView: RecyclerView
    private lateinit var categoryAdapter: RecyclerView.Adapter<*>
    private lateinit var categoryLayoutManager: GridLayoutManager

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        categoryViewModel =
                ViewModelProvider(this).get(CategoryViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_category, container, false)
//        val textView: TextView = root.findViewById(R.id.text_category)
//        categoryViewModel.text.observe(viewLifecycleOwner, Observer {
//            textView.text = it
//        })

        categoryLayoutManager = GridLayoutManager(context, 2)
        categoryLayoutManager.spanSizeLookup = CategorySpanSizeLookUp(categoryViewModel.categories)

        categoryAdapter = CategoryAdapter(requireContext(), categoryViewModel.categories)

        categoryRecyclerView = root.findViewById<RecyclerView>(R.id.category_recylerView).apply {
            setHasFixedSize(true)
            layoutManager = categoryLayoutManager
            adapter = categoryAdapter
            addItemDecoration(CategoryItemDecoration())
        }

        root.findViewById<Button>(R.id.btn_body_part).setOnClickListener(this)
        root.findViewById<Button>(R.id.btn_function).setOnClickListener(this)
        root.findViewById<Button>(R.id.btn_target).setOnClickListener(this)
        root.findViewById<Button>(R.id.btn_ingredient).setOnClickListener(this)

        return root
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_body_part -> {
                categoryRecyclerView.scrollToPosition(0)
            }
            R.id.btn_function -> {

            }
            R.id.btn_target -> {

            }
            R.id.btn_ingredient -> {

            }
        }
    }
}