package com.example.annyeong.ui.category

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.annyeong.utils.toPx

class CategoryProductsDecoration(): RecyclerView.ItemDecoration() {

    val paddingHorizontal = 23.toPx()
    val itemSpacingHorizontal = 7.toPx()
    val itemSpacingVertical = 18.toPx()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildAdapterPosition(view)
        val isLeftItem = position % 2 == 0

        if (isLeftItem) {
            outRect.top = itemSpacingVertical
            outRect.left = paddingHorizontal + itemSpacingHorizontal
            outRect.bottom = itemSpacingVertical
            outRect.right = itemSpacingHorizontal
        } else {
            outRect.top = itemSpacingVertical
            outRect.left = itemSpacingHorizontal
            outRect.bottom = itemSpacingVertical
            outRect.right = paddingHorizontal + itemSpacingHorizontal
        }
    }
}