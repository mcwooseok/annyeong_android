package com.example.annyeong.ui.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CategoryViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "카테고리입니다"
    }
    val text: LiveData<String> = _text

    val categories: List<Category> = listOf(
            Category("", "신체 부위별\n제품 찾기", CATEGORY_VIEW_TYPE_HEADER),
            Category("", "기억력 개선", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "눈 건강", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "피부 건강", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "심혈관 / 콜레스테롤", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "간 건강", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "장 건강", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "비뇨기 / 성기능", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "관절 / 뼈 / 치아", CATEGORY_VIEW_TYPE_MAIN_ITEM),

            Category("", "기능별\n제품 찾기", CATEGORY_VIEW_TYPE_HEADER),
            Category("", "스트레스 / 수면장애", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "향산화", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "면역", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "운동 / 다이어트", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "갱년기증상완화", CATEGORY_VIEW_TYPE_MAIN_ITEM),

            Category("", "대상별\n제품 찾기", CATEGORY_VIEW_TYPE_HEADER),
            Category("", "", CATEGORY_VIEW_TYPE_SUB_ITEM, listOf(
                    Category("", "키즈", CATEGORY_VIEW_TYPE_SUB_ITEM),
                    Category("", "여성", CATEGORY_VIEW_TYPE_SUB_ITEM),
                    Category("", "남성", CATEGORY_VIEW_TYPE_SUB_ITEM),
                    Category("", "임산부", CATEGORY_VIEW_TYPE_SUB_ITEM),
                    Category("", "노인", CATEGORY_VIEW_TYPE_SUB_ITEM),
                    Category("", "군인", CATEGORY_VIEW_TYPE_SUB_ITEM)
            )),

            Category("", "영양제 성분으로\n제품 찾기", CATEGORY_VIEW_TYPE_HEADER),
            Category("", "비타민", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "오메가", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "유산균", CATEGORY_VIEW_TYPE_MAIN_ITEM),
            Category("", "미네랄", CATEGORY_VIEW_TYPE_MAIN_ITEM)
    )
}