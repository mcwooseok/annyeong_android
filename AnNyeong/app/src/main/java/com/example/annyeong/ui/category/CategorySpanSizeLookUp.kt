package com.example.annyeong.ui.category

import androidx.recyclerview.widget.GridLayoutManager
import com.example.annyeong.model.Product

class CategorySpanSizeLookUp(
        val dataSource: List<Category>
): GridLayoutManager.SpanSizeLookup() {
    override fun getSpanSize(position: Int): Int {
        return if (dataSource[position].type == CATEGORY_VIEW_TYPE_MAIN_ITEM) {
            1
        } else {
            2
        }
    }
}