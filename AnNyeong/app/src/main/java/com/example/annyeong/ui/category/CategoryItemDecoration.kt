package com.example.annyeong.ui.category

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.annyeong.utils.toPx

class CategoryItemDecoration(): RecyclerView.ItemDecoration() {

    val paddingHorizontal = 22.toPx()
    val paddingVertical = 40.toPx()
    val itemSpacingHorizontal = 8.toPx()
    val itemSpacingVertical = 8.toPx()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildAdapterPosition(view)
        val lp = view.layoutParams as GridLayoutManager.LayoutParams
        val spanIndex = lp.spanIndex

        val dataSource = (parent.adapter as CategoryAdapter).dataSource

        when (dataSource[position].type) {
            CATEGORY_VIEW_TYPE_HEADER -> {
                outRect.top = 0
                outRect.left = 0
                outRect.bottom = paddingVertical
                outRect.right = 0
            }
            CATEGORY_VIEW_TYPE_MAIN_ITEM -> {
                outRect.top = itemSpacingVertical
                outRect.bottom = itemSpacingVertical

                if (spanIndex == 0) {
                    outRect.left = paddingHorizontal + itemSpacingHorizontal
                    outRect.right = itemSpacingHorizontal
                } else {
                    outRect.left = itemSpacingHorizontal
                    outRect.right = paddingHorizontal + itemSpacingHorizontal
                }
            }
            CATEGORY_VIEW_TYPE_SUB_ITEM -> {
                outRect.top = 0
                outRect.left = 0
                outRect.bottom = 0
                outRect.right = 0
            }
        }
    }
}