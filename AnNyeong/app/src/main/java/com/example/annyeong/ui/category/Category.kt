package com.example.annyeong.ui.category

data class Category(
        val thumbnailPath: String,
        val title: String,
        val type: Int,
        var categories: List<Category>? = null
) {

}