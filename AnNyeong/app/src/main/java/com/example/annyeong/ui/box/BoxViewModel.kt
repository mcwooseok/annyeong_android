package com.example.annyeong.ui.box

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BoxViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "안녕박스입니다"
    }
    val text: LiveData<String> = _text
}