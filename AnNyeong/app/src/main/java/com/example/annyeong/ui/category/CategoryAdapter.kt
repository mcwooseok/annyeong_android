package com.example.annyeong.ui.category

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.annyeong.R
import com.example.annyeong.utils.toPx

const val CATEGORY_VIEW_TYPE_HEADER = 0
const val CATEGORY_VIEW_TYPE_MAIN_ITEM = 1
const val CATEGORY_VIEW_TYPE_SUB_ITEM = 2

class CategoryAdapter(
        val context: Context,
        val dataSource: List<Category>
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemViewType(position: Int): Int {
        super.getItemViewType(position)
        return dataSource[position].type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == CATEGORY_VIEW_TYPE_HEADER) {
            val baseView: View =
                    LayoutInflater.from(context).inflate(R.layout.layout_category_header, parent, false)
            return CategoryHeaderItemHolder(baseView)
        } else if (viewType == CATEGORY_VIEW_TYPE_MAIN_ITEM) {
            val baseView: View =
                    LayoutInflater.from(context).inflate(R.layout.layout_category_main_item, parent, false)
            return CategoryMainItemHolder(baseView)
        } else {
            val baseView: View =
                    LayoutInflater.from(context).inflate(R.layout.layout_category_sub_cell, parent, false)
            val subDataSource = dataSource.filter { it.categories != null }
            var holder = CategorySubCellHolder(baseView, context, subDataSource.first().categories!!)
            holder.recyclerView.let {
                it?.setHasFixedSize(true)
                it?.layoutManager = holder.horizontalLayoutManager
                it?.adapter = holder.categoryAdapter
                it?.addItemDecoration(CategorySubItemDecoration())
            }
            return holder
        }
    }

    override fun getItemCount(): Int {
        return dataSource.count()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as? CategoryHeaderItemHolder).let {
            it?.headerTextView?.text = dataSource[position].title
        }
        (holder as? CategoryMainItemHolder).let {
            it?.thumbnailImageView?.setImageDrawable(null)
            it?.titleTextView?.text = dataSource[position].title
            it?.titleTextView?.setOnClickListener {
                var intent = Intent(context, CategoryProductsActivity::class.java)
                context.startActivity(intent)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)
    }

    class CategoryHeaderItemHolder(
            itemView: View
    ): RecyclerView.ViewHolder(itemView) {
        var headerTextView = itemView.findViewById<TextView>(R.id.tv_header)
    }

    class CategoryMainItemHolder(
            itemView: View
    ): RecyclerView.ViewHolder(itemView) {
        var thumbnailImageView = itemView.findViewById<ImageView>(R.id.iv_category_main_thumbnail)
        var titleTextView = itemView.findViewById<TextView>(R.id.tv_category_main_title)
    }

    class CategorySubCellHolder(
        itemView: View,
        context: Context,
        dataSource: List<Category>
    ): RecyclerView.ViewHolder(itemView) {
        var recyclerView = itemView.findViewById<RecyclerView>(R.id.category_sub_recyclerView)
        var categoryAdapter = CategorySubAdapter(context, dataSource)
        var horizontalLayoutManager: LinearLayoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
    }
}