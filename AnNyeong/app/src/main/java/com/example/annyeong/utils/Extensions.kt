package com.example.annyeong.utils

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue

fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()
fun Int.toDp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()