package com.example.annyeong.ui.category

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.annyeong.utils.toPx

class CategorySubItemDecoration(): RecyclerView.ItemDecoration() {

    val paddingHorizontal = 23.toPx()
    val itemSpacingHorizontal = 7.toPx()

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        val position = parent.getChildAdapterPosition(view)
        val dataSource = (parent.adapter as CategorySubAdapter).dataSource

        if (position == 0) {
            outRect.top = 0
            outRect.left = paddingHorizontal + itemSpacingHorizontal
            outRect.bottom = 0
            outRect.right = itemSpacingHorizontal
        } else if (position == dataSource.count() - 1) {
            outRect.top = 0
            outRect.left = itemSpacingHorizontal
            outRect.bottom = 0
            outRect.right = paddingHorizontal + itemSpacingHorizontal
        } else {
            outRect.top = 0
            outRect.left = itemSpacingHorizontal
            outRect.bottom = 0
            outRect.right = itemSpacingHorizontal
        }
    }
}