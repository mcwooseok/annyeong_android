package com.example.annyeong.model

import com.saber.stickyheader.stickyData.StickyMainData

class Product(
    val thumbnailPath: String? = "",
    val isAdded: Boolean,
    val tagType: Int? = 0,
    val tagThumbnailPath: String? = "",
    val title: String? = "",
    val score: Float? = 4.6f,
    val reviewCount: Int? = 95
): StickyMainData {

}